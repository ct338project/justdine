package servlet;

import servlet.exception.BadParamException;
import servlet.exception.MissingParamException;
import servlet.exception.HttpException;
import servlet.exception.UnauthorizedException;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet help functions.
 * 
 * This is a class with a bunch of helper methods
 * to help with controlling the output ot the user.
 * 
 * @author Adrian
 */
public class Controller { 
    private static final Logger LOG = Logger.getLogger(Controller.class.getName());
    
    /**
     * Fail a HTTP request and display an error page (with exception)
     * @param request
     * @param response
     * @param code
     * @param ex
     * @throws ServletException
     * @throws IOException 
     */
    public static void fail(HttpServletRequest request, HttpServletResponse response, final HttpException ex) throws ServletException, IOException {
        display("error.jsp", new HashMap<String, Object>() {{
            put("errorCode", ex.statusCode);
            put("errorMessage", ex.statusMessage);
            put("exceptionMessage", ex.getMessage());
        }}, request, response);
    }
    
    /**
     * Fail a HTTP request with just an error code.
     * @param request
     * @param response
     * @param code
     * @throws ServletException
     * @throws IOException 
     */
    public static void fail(HttpServletRequest request, HttpServletResponse response, int code) throws ServletException, IOException {
        fail(request, response, new HttpException(code));
    }
    
    /**
     * Fail with a 500.
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException 
     */
    public static void fail(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        fail(request, response, 500);
    }
    
    /**
     * Display a JSP page with data.
     * @param page  String  The name of the JSP page
     * @param map   HashMap The data to pass to the template. 
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException 
     */
    public static void display(String page, HashMap<String, Object> map, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if(map != null) {
            Iterator it = map.entrySet().iterator();
            
            Set keys = map.keySet();
            LOG.info(String.format("Display page '%s' with attributes: %s", page, Arrays.toString(keys.toArray(new String[keys.size()]))));
            
            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry)it.next();
                request.setAttribute((String) pairs.getKey(), (Object) pairs.getValue());
            }
        } else LOG.info(String.format("Displaying page '%s'.", page));
        
        request.getRequestDispatcher(page).forward(request, response);
    }
    
    /**
     * Display a JSP page without any data.
     * @param page
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException 
     */
    public static void display(String page, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        display(page, null, request, response);
    }
    
    /**
     * Log a debug message.
     * @param message 
     */
    public static void log(String message) { LOG.log(Level.WARNING, message); }
    public static void log(Throwable message) { LOG.log(Level.WARNING, "[EXCEPTION] ", message); }
    
    /**
     * Get the page from the request via the URL query parameters.
     * 
     * e.g. /restaurant?page=new => "new"
     * 
     * Useful for switching over an incoming request:
     * 
     * switch(Controller.page("request")) {
     *      case "new":
     *          createNewRestaurant()
     *      break;
     * }
     * 
     * If an action parameter is present, it returns:
     * GET /restaurant?page=new&action=owner
     * -> "new:owner"
     * 
     * @param request
     * @return 
     */
    public static String page(HttpServletRequest request) {
        String pageType = (String) param(request, "page");
        String action = (String) param(request, "action");
        
        if(pageType == null) pageType = "";
        
        if(action != null) return pageType + ":" + action;
        else return pageType;
    }
    
    /**
     * Get the page from the URL query parameters and with a fallback default page.
     * @param request
     * @param defaultPage
     * @return 
     */
    public static String page(HttpServletRequest request, String defaultPage) {
        String requestPage = page(request);
        return requestPage != "" ? requestPage : defaultPage;
    }
    
    /**
     * Parse incoming query parameter page and if not exists, 404.
     * @param request
     * @param response
     * @return
     * @throws ServletException 
     */
    public static String page(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String newPage = page(request);
        
        if(newPage == "") Controller.fail(request, response, 404);
        else return newPage;
        
        return null;
    }
    
    /**
     * Parameter parser for a HTTP request.
     * 
     * This takes in a request and returns a parsed parameter. For example,
     * let's take an ID parameter form the request, parse the int and return it.
     * 
     * Integer id = Controller.param(request, "id", true, new ParamParser<Integer>{
     *      public Integer parse(String value) {
     *          return Integer.parseInt(value);
     *      }
     * });
     * 
     * @param <T>
     * @param request
     * @param name
     * @param required
     * @param parser
     * @return
     * @throws Exception if parameter is required but not present
     */
    public static <T extends Object> T param(HttpServletRequest request, String name, boolean required, ParamParser<T> parser) throws BadParamException, MissingParamException {
        String value = request.getParameter(name);
        
        if(value != null) {
            try {
                return parser.parse(value);
            } catch(BadParamException e) {
                throw e;
            } catch(Exception e) {
                throw new BadParamException(e.getMessage());
            }
        } else if(required && value == null) throw new MissingParamException(name);
        else return null;
    }
    
    /**
     * Parse an optional parameter.
     * 
     * See Controller.param(HttpServletRequest, String, boolean, ParamParser<T>);
     * 
     * @param <T>
     * @param request
     * @param name
     * @param parser
     * @return
     * @throws Exception 
     */
    public static <T extends Object> T param(HttpServletRequest request, String name, ParamParser<T> parser) throws BadParamException {
        // Since the MissingParamException is only thrown if the `required` variable is
        // true, we can safely ignore it because it can never happen.
        try {
            return param(request, name, false, parser);
        } catch(MissingParamException ex) {
            return null;
        } 
    }
    
    /**
     * Get a parameter from a request object.
     * @param request
     * @param name
     * @return 
     */
    public static Object param(HttpServletRequest request, String name) {
        return request.getParameter(name);
    }
    
    /**
     * Interface to parsing parameters.
     * @param <T> 
     */
    public interface ParamParser<T> {
        public T parse(String value) throws BadParamException;
    }
    
    // Built in int parser
    public static ParamParser<Integer> IntegerParser = new ParamParser<Integer>() {
        public Integer parse(String value) {
            return Integer.parseInt(value);
        }
    };
    
    /**
     * Set an attribute on the request object.
     * @param request
     * @param name
     * @param value 
     */
    public static void attr(HttpServletRequest request, String name, Object value) {
        request.setAttribute(name, value);
    }
    
    /**
     * Get a value from the request session by name.
     * @param request
     * @param name
     * @return 
     */
    public static Object session(HttpServletRequest request, String name) {
        return request.getSession().getAttribute(name);
    }
    
    /**
     * Redirect a request to another path.
     * 
     * @param response
     * @param path 
     */
    public static void redirect(HttpServletResponse response, String path) {
        response.setStatus(response.SC_MOVED_TEMPORARILY);
        response.setHeader("Location", "/JustDine" + path);
    }
    
    /**
     * Test if a request has all the required parameters.
     * 
     * @param request
     * @param attributes
     * @return
     * @throws MissingParamException is a parameter in the required list is missing. 
     */
    public static boolean hasRequired(HttpServletRequest request, String[] attributes) throws MissingParamException {
        for(String attribute : attributes) {
            if(request.getParameter(attribute) == null) throw new MissingParamException(attribute);
        }
        
        return true;
    }
    
    public static boolean isLoggedIn(HttpServletRequest request, boolean required) throws UnauthorizedException {
        if(request.getSession().getAttribute("user") != null) return true;
        else if(required) throw new UnauthorizedException();
        else return false;
    }
    
    /**
     * Test is a user is logged in to current session.
     * @param request
     * @return
     * @throws UnauthorizedException is the user is not logged in.
     */
    public static boolean isLoggedIn(HttpServletRequest request) throws UnauthorizedException {
        return isLoggedIn(request, true);
    }
}
