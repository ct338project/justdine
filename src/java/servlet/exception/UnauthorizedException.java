/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.exception;

/**
 *
 * @author Adrian
 */
public class UnauthorizedException extends HttpException {
    public UnauthorizedException() {
        super(401);
    }
}
