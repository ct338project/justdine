/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.exception;

/**
 *
 * @author Adrian
 */
public class FormException extends Exception {

    /**
     * Creates a new instance of <code>FormException</code> without detail
     * message.
     */
    public FormException() {
    }

    /**
     * Constructs an instance of <code>FormException</code> with the specified
     * detail message.
     *
     * @param msg the detail message.
     */
    public FormException(String msg) {
        super(msg);
    }
}
