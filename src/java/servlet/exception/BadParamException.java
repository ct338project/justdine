/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.exception;

/**
 *
 * @author Adrian
 */
public class BadParamException extends HttpException {
    public BadParamException(String name) {
        super(400, "Error parsing parameter '" + name + "'.");
    }
}
