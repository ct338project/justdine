/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.exception;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Adrian
 */
public class HttpException extends Exception {
    public static final Map<Integer, String> errors;
    
    static
    {
        errors = new HashMap<Integer, String>();
        errors.put(400, "Bad request.");
        errors.put(401, "Unauthorized.");
        errors.put(404, "Page Not Found.");
        errors.put(500, "Internal Server Error");
    }
    
    public int statusCode;
    public String statusMessage;
    
    /**
     * Creates a new instance of <code>HttpException</code> without detail
     * message.
     */
    public HttpException(int code, String message) {
        super(message);
        
        // Set the HTTP status code
        this.statusCode = code;
        
        // Set the HTTP status message
        this.statusMessage = errors.get(code);
    }
    
    public HttpException(int code) {
        this(code, null);
    }
    
    public HttpException() {
        this(500);
    }
}
