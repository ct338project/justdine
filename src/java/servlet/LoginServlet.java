/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import entity.User;
import static entity.User.password;
import servlet.exception.HttpException;
import servlet.exception.IncorrectCredentialsException;
import session.UserFacade;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Sean
 */
@WebServlet(name = "LoginServlet", urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {

    @EJB
    private UserFacade userFacade;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, UnsupportedEncodingException {

        try {
            if(Controller.hasRequired(request, new String[] { "email", "password" })) {
                String email = (String) Controller.param(request, "email");
                String password = (String) Controller.param(request, "password");
                
                // Login the user, if it fails see throw class IncorrectCredentialsException
                userFacade.login(request, email, password);
                Controller.redirect(response, "/");
            }
        } catch (final IncorrectCredentialsException ex) {
            Controller.display("login.jsp", new HashMap<String, Object>() {{
                put("error", ex);
            }}, request, response);
        } catch (HttpException ex) {
            Controller.fail(request, response, ex);
        }

    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, java.io.IOException {

        Controller.display("login.jsp", request, response);

    }

}
