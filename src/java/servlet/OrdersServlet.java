/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import entity.Restaurant;
import entity.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import servlet.exception.HttpException;
import servlet.exception.UnauthorizedException;
import session.RestaurantFacade;

/**
 *
 * @author Adrian
 */
@WebServlet(name = "OrdersServlet", urlPatterns = {"/orders"})
public class OrdersServlet extends HttpServlet {
    @EJB
    private RestaurantFacade restaurantFacade;

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            if(Controller.isLoggedIn(request)) {
                switch(Controller.page(request)) {
                    // Display orders for a restaurant
                    case "restaurant":
                        if(Controller.hasRequired(request, new String[] { "restaurant" })) {
                            Integer id = Controller.param(request, "restaurant", Controller.IntegerParser);
                            
                            // Get the restaurant
                            final Restaurant restaurant = restaurantFacade.find(id);
                            
                            // Make sure the restaurant exists.
                            if(restaurant == null) throw new HttpException(404);
                            
                            // Ensure they're the owner of the restaurant
                            if(!restaurant.isOwner((User) Controller.session(request, "user"))) throw new HttpException(401, "You don't own this restaurant.");
                            
                            Controller.display("orders.jsp", new HashMap<String, Object>() {{
                                put("orders", restaurant.getCustomerOrderCollection());
                            }}, request, response);
                        }
                    break;
                        
                    // Display orders for a user
                    default:
                        final User user = (User) Controller.session(request, "user");
                        Controller.display("orders.jsp", new HashMap<String, Object>() {{
                                put("orders", user.getCustomerOrderCollection());
                                put("forUser", true);
                        }}, request, response);
                }
            }
        } catch (UnauthorizedException ex) {
            Controller.redirect(response, "/login");
        } catch (HttpException ex) {
            Controller.fail(request, response, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Controller.fail(request, response, 404);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
