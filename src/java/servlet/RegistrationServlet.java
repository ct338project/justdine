package servlet;

import entity.User;
import servlet.exception.HttpException;
import servlet.exception.IncorrectCredentialsException;
import servlet.exception.InternalException;
import servlet.exception.MissingParamException;
import session.UserFacade;
import java.io.*;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.ejb.EJB;
import javax.servlet.annotation.WebServlet;
import servlet.exception.FormException;

/**
 *
 * @author Sean
 */
@WebServlet(name = "RegistrationServlet", urlPatterns = {"/registration"})
public class RegistrationServlet extends HttpServlet {

    @EJB
    private UserFacade userFacade;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            // Test if the required parameters exist
            if (Controller.hasRequired(request, new String[]{"name", "email", "password", "confirmpassword"})) {

                String name = (String) Controller.param(request, "name");
                String email = (String) Controller.param(request, "email");
                String password = (String) Controller.param(request, "password");
                String confirmPassword = (String) Controller.param(request, "confirmpassword");
                String salt = User.salt(); // Generate a user salt
                
                // If the user exists already with that email, fail
                if(userFacade.findUser(email) != null) throw new FormException("User already exists.");

                // The passwords match
                if (password.equals(confirmPassword)) {
                    // Hash the password
                    String hashword = User.password(password, salt);
                    User user = new User();

                    // Update the attributes on the user instance
                    user.setName(name);
                    user.setEmail(email);
                    user.setSalt(salt);
                    user.setPassword(hashword);

                    // Create the user
                    userFacade.create(user);

                    // Log them in automatically
                    userFacade.login(request, email, password);

                    // Redirect to homepage
                    Controller.redirect(response, "/profile");
                } throw new FormException("Passwords do not match.");

            }
        } catch (NoSuchAlgorithmException | IOException | InternalException | IncorrectCredentialsException e) {
            // 500 for server errors
            Controller.fail(request, response);
        } catch (final MissingParamException | FormException ex) {
            Controller.display("registration.jsp", new HashMap<String, Object>() {
                {
                    put("error", ex);
                }
            }, request, response);
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Controller.display("registration.jsp", request, response);

    }
}
