/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import entity.Restaurant;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Adrian
 */
@WebServlet(name = "EntityInterface", urlPatterns = {"/EntityInterface"})
public class EntityInterface extends HttpServlet {
    @Resource
    private javax.transaction.UserTransaction utx;
    @PersistenceContext(unitName = "web-jpaPU")
    private EntityManager em;
    
    private static final Logger LOG = Logger.getLogger(EntityInterface.class.getName());

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        
        try {
            out.println("GO AWAY, FUCK OFF.");
        } finally {
            out.close();
        }
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        PrintWriter out = response.getWriter();
        String entityName = request.getParameter("entity");
        
        if(entityName == null) {
            out.println("No entity specified, dumbass");
            out.close();
        }
        
        try {
            Class entity = Class.forName("entity." + capitalize(entityName));
            
            Object entityInstance = entity.newInstance();
            
            Enumeration<String> parameterNames = request.getParameterNames();

            while (parameterNames.hasMoreElements()) {
                String paramName = parameterNames.nextElement();
                if(paramName.equals("entity")) continue;
                
                // Get the value of the parameter
                String paramValue = request.getParameter(paramName);
                
                // Get the param type
                Class paramType = this.getParamType(paramValue);
                
                LOG.info("Paramter Name: " + paramName + ", value: " + paramValue + ", type: " + paramType.toString());
                
                // Get the setter
                Method setter = entity.getDeclaredMethod("set" + capitalize(paramName), paramType);
                
                // Execute the setter on the entity instance
                switch(paramType.toString()) {
                    case "int": 
                        int value = Integer.parseInt(paramValue);
                        setter.invoke(entityInstance, value);
                    break;
                       
                    case "class java.lang.String":
                        setter.invoke(entityInstance, paramValue);
                    break;
                }
            }
            
            // Save the entity to the database
            this.persist(entityInstance);
            
            out.write("SUCCESS, MOFO");
            out.close();
        } catch (InstantiationException ex) {
            Logger.getLogger(EntityInterface.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(EntityInterface.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(EntityInterface.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchMethodException ex) {
            Logger.getLogger(EntityInterface.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SecurityException ex) {
            Logger.getLogger(EntityInterface.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalArgumentException ex) {
            Logger.getLogger(EntityInterface.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvocationTargetException ex) {
            Logger.getLogger(EntityInterface.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static String capitalize(String str) {
        return Character.toUpperCase(str.charAt(0)) + str.substring(1).toLowerCase();
    }
    
    public static Class getParamType(String param) {
        try {
            Integer.parseInt(param);
            
            return int.class;
        } catch(NumberFormatException ex) {
            return String.class;
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

    public void persist(Object object) {
        try {
            utx.begin();
            em.persist(object);
            utx.commit();
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", e);
            throw new RuntimeException(e);
        }
    }

}
