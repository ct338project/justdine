/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import entity.CustomerOrder;
import entity.Menu;
import entity.MenuCategory;
import entity.MenuItem;
import entity.Restaurant;
import entity.User;
import servlet.exception.HttpException;
import servlet.exception.MissingParamException;
import servlet.exception.UnauthorizedException;
import session.MenuCategoryFacade;
import session.MenuFacade;
import session.RestaurantFacade;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import servlet.exception.BadParamException;
import session.CustomerOrderFacade;
import session.MenuItemFacade;

/**
 *
 * @author Adrian
 */
@WebServlet(name = "RestaurantServlet", urlPatterns = {"/restaurant"})
public class RestaurantServlet extends HttpServlet {
    @EJB
    private MenuItemFacade menuItemFacade;
    @EJB
    private CustomerOrderFacade customerOrderFacade;
    @EJB
    private MenuCategoryFacade menuCategoryFacade;
    @EJB
    private MenuFacade menuFacade;
    @EJB
    private RestaurantFacade restaurantFacade;
    
    
    public Controller.ParamParser<Restaurant> RestaurantParser = new Controller.ParamParser<Restaurant>() {
        public Restaurant parse(String value) throws BadParamException {
            // Parse the ID, if it's a string, it will throw an exception
            int id = Integer.parseInt(value);

            // Find within the database
            Restaurant restaurant = restaurantFacade.find(id);
            
            if(restaurant == null) throw new BadParamException("Restaurant does not exist!");
            else return restaurant;
        }
    };
    
    public Controller.ParamParser<MenuItem> MenuItemParser = new Controller.ParamParser<MenuItem>() {
        public MenuItem parse(String value) throws BadParamException {
            int id = Integer.parseInt(value);
            
            MenuItem item = menuItemFacade.find(id);
            
            if(item == null) throw new BadParamException("Item does not exist!");
            else return item;
        }
    };      
    
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        try {
            
            // Router
            String page = Controller.page(request, "menu");
                
            switch(page) {
                // Creating a new restaurant page
                case "new":
                    // You have to be logged in to create a restaurant
                    if(Controller.isLoggedIn(request))
                        Controller.display("restaurantNew.jsp", request, response);
                break;
                    
                // Add an order to a user
                case "order:add":
                    addItemToOrder(request, response);
                break;
                    
                default:
                    // Get the restaurant from the ID parameter
                    Restaurant restaurant = Controller.param(request, "id", true, RestaurantParser);
                    displayRestaurantPage(restaurant, page, request, response);
            }
        } catch(UnauthorizedException ex) {
            Controller.redirect(response, "/login");
        } catch(HttpException ex) {
            Controller.fail(request, response, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        // Get the page from the post request
        switch(Controller.page(request, response)) {
            // Handle POST /restaurant?page=new
            
            case "new":
                try {
                    // Check if the request has the required parameters and the user is logged in.
                    if(Controller.isLoggedIn(request) && Controller.hasRequired(request, new String[] { "name", "description", "category" })) {
                        String name = (String) Controller.param(request, "name");
                        String description = (String) Controller.param(request, "description");
                        String category = (String) Controller.param(request, "category");
                        String photo = (String) Controller.param(request, "photo"); // Allow photo url for the data generator
                        Integer rating = Controller.param(request, "rating", Controller.IntegerParser); // Allow photo url for the data generator

                        // First of all create the restaurant
                        Restaurant newRestaurant = new Restaurant();
                        
                        newRestaurant.setName(name);
                        newRestaurant.setDescription(description);
                        newRestaurant.setCategory(category);
                        newRestaurant.setOwner((User) request.getSession().getAttribute("user"));
                        if(photo != null) newRestaurant.setPhoto(photo);
                        if(rating != null) newRestaurant.setRating(rating);
                        
                        // Create the restaurants menu
                        Menu menu = new Menu();
                        
                        // Add some menu categories
                        menu.addCategory(new MenuCategory("Starters", 1));
                        menu.addCategory(new MenuCategory("Mains", 2));
                        menu.addCategory(new MenuCategory("Desserts", 3));
                        
                        menu.setFkRestaurant(newRestaurant);
                        newRestaurant.setMenu(menu);
                        
                        restaurantFacade.create(newRestaurant);
                        
                        Controller.redirect(response, "/restaurant?id=" + newRestaurant.getPkId());
                    }
                } catch (final MissingParamException ex) {
                    Controller.display("restaurantNew.jsp", new HashMap<String, Object>() {{
                        put("error", ex);
                    }}, request, response);
                } catch (HttpException ex) {
                    Controller.fail(request, response, ex);
                }
            break;
        }
    }
    
    protected void displayRestaurantPage(final Restaurant restaurant, String page, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        try {
            // Check to see if the user has an order
            if(Controller.isLoggedIn(request, false)) {
                Controller.attr(request, "order", customerOrderFacade.getCustomerOrderForRestaurant((User) Controller.session(request, "user"), restaurant));
            }
        } catch (UnauthorizedException ex) {
            // this should never happen, bad method planning by myself.
            Controller.fail(request, response, 500);
        }
        
        switch(page) {  
            case "order":
                displayOrder(restaurant, request, response);
            break;

            case "info":
                displayInfo(restaurant, request, response);
            break;

            case "menu":
                displayMenu(restaurant, request, response);
            break;
        }
    }
    
    protected void displayMenu(final Restaurant restaurant, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Controller.display("restaurantMenu.jsp", new HashMap<String, Object>() {{
            put("restaurant", restaurant);
        }}, request, response);
    }
    
    protected void displayOrder(final Restaurant restaurant, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {        
        Controller.display("restaurantOrder.jsp", new HashMap<String, Object>() {{
            put("restaurant", restaurant);
        }}, request, response);
    }

    protected void displayInfo(final Restaurant restaurant, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Controller.display("restaurantInfo.jsp", new HashMap<String, Object>() {{
            put("restaurant", restaurant);
        }}, request, response);
    }
    
    /**
     * Add an item to the currently logged in user.
     * 
     * Steps involved in creating an order:
     * 
     * 1. Check to see if a user has already started an order with that restaurant.
     * -> 1.1 If not, create an order for a user with that restaurant.
     * 2. Check to see if that item exists and is an item of that restaurant's menu.
     * 3. Check to see if the user has already added this item, if so increment quantity.
     * 4. 
     * 
     * @param request
     * @param response 
     */
    protected void addItemToOrder(HttpServletRequest request, HttpServletResponse response) throws HttpException {
        if(Controller.hasRequired(request, new String[] { "restaurant", "item" }) && Controller.isLoggedIn(request)) {
            User user = (User) Controller.session(request, "user");
            
            // Get the restaurant from the request
            Restaurant restaurant = Controller.param(request, "restaurant", true, RestaurantParser);
            
            // Get the user's current order, if they have one
            CustomerOrder order = customerOrderFacade.getCustomerOrderForRestaurant(user, restaurant);
            
            // If the order if null, create a customer order
            if(order == null) {
                order = customerOrderFacade.createCustomerOrderForRestaurant(user, restaurant);
                customerOrderFacade.create(order);
            }
            
            MenuItem item = Controller.param(request, "item", true, MenuItemParser);
            
            // Check to see if the restaurant menu has the item
            if(!restaurant.getMenu().hasItem(item)) throw new HttpException(400, "Menu item does not belong to the restaurant's menu!");
            
            // Add the item to the order
            order.addItem(item);
            
            // Update the order
            customerOrderFacade.edit(order);
            
            Controller.redirect(response, "/restaurant?page=menu&id=" + restaurant.getPkId());
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
