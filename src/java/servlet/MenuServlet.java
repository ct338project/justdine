/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import entity.Menu;
import entity.MenuCategory;
import entity.MenuItem;
import entity.Restaurant;
import entity.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import servlet.exception.HttpException;
import session.MenuCategoryFacade;
import session.MenuItemFacade;
import session.RestaurantFacade;

/**
 *
 * @author Adrian
 */
@WebServlet(name = "MenuServlet", urlPatterns = {"/menu"})
public class MenuServlet extends HttpServlet {
    @EJB
    private MenuItemFacade menuItemFacade;
    @EJB
    private MenuCategoryFacade menuCategoryFacade;
    @EJB
    private RestaurantFacade restaurantFacade;
    
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        try {
            switch(Controller.page(request, response)) {
                case "item:create":

                    HashMap<String, Object> data = extractItemParameters(request);
                    Controller.display("menuAddItem.jsp", data, request, response);
                break;
                    
                default:
                    throw new HttpException(404);
            }
        } catch(HttpException e) {
            Controller.fail(request, response, e);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        try {
            switch(Controller.page(request, response)) {
                case "item:create":
                    if(Controller.hasRequired(request, new String[] { "price", "title", "subtitle" })) {
                        HashMap<String, Object> data = extractItemParameters(request);
                        Restaurant restaurant = (Restaurant) data.get("restaurant");
                        MenuCategory category = (MenuCategory) data.get("category");

                        String itemTitle = (String) Controller.param(request, "title");
                        String itemSubTitle = (String) Controller.param(request, "subtitle");
                        Double itemPrice = Controller.param(request, "price", new Controller.ParamParser<Double>() {
                            public Double parse(String value) {
                                return Double.valueOf(value);
                            }
                        });

                        MenuItem item = new MenuItem();
                        item.setTitle(itemTitle);
                        item.setDescription(itemSubTitle);
                        item.setFKMenuCategory(category);
                        item.setPrice(itemPrice);

                        menuItemFacade.create(item);

                        Controller.redirect(response, "/restaurant?id=" + restaurant.getPkId());
                    }
                break;
                    
                default:
                    throw new HttpException(404);
            }
        } catch(HttpException e) {
            Controller.fail(request, response, e);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
    
    public HashMap<String, Object> extractItemParameters(HttpServletRequest request) throws HttpException {
        if(Controller.hasRequired(request, new String[] { "restaurant", "category" }) && Controller.isLoggedIn(request)) {
            // Get the currently logged in user
            User user = (User) Controller.session(request, "user");

            // Parse the incoming restaurant and category ID
            Integer restaurantId = Controller.param(request, "restaurant", Controller.IntegerParser);
            Integer categoryId = Controller.param(request, "category", Controller.IntegerParser);

            // Find the restaurant
            final Restaurant restaurant = restaurantFacade.find(restaurantId);

            if(restaurant == null) throw new HttpException(400, "Restaurant does not exist.");

            // Ensure the user is the owner
            if(restaurant.isOwner(user)) {
                // Get the menu category
                final MenuCategory category = menuCategoryFacade.find(categoryId);

                // Ensure it exists
                if(category == null) throw new HttpException(400, "Category does not exist.");

                // Get the restarurant menu and
                Menu restaurantMenu = restaurant.getMenu();

                // Check to see if the category belongs to the restaurant ID
                if(restaurantMenu.hasCategory(category)) {
                    // Everything checks out
                     return new HashMap<String, Object>() {{
                         put("restaurant", restaurant);
                         put("category", category);
                     }};
                } else throw new HttpException(401, "The category does not below to the supplied restaurant!");                            
            } else throw new HttpException(401, "You don't own this restaurant! Sneaky bastard.");
        }
        
        return null;
    }
}
