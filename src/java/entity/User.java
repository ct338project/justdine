/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.HexBinaryAdapter;
import session.CustomerOrderFacade;

/**
 *
 * @author Adrian
 */
@Entity
@Table(name = "User", uniqueConstraints = @UniqueConstraint(columnNames = {"email"}))
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "User.findAll", query = "SELECT u FROM User u"),
    @NamedQuery(name = "User.findByPkId", query = "SELECT u FROM User u WHERE u.pkId = :pkId"),
    @NamedQuery(name = "User.findByName", query = "SELECT u FROM User u WHERE u.name = :name"),
    @NamedQuery(name = "User.findByEmail", query = "SELECT u FROM User u WHERE u.email = :email")})
public class User implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "PK_ID")
    private Integer pkId;
    @Size(max = 255)
    @NotNull
    @Column(name = "name")
    private String name;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 255)
    @NotNull
    @Column(name = "email")
    private String email;
    @Size(max = 255)
  @Column(name = "salt")
    @NotNull
    private String salt;
    @Size(max = 255)
    @Column(name = "password")
    @NotNull
    private String password;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "fKUser")
    private Collection<CustomerOrder> customerOrderCollection;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "fKOwner")
    private Collection<Restaurant> restaurantCollection;
   

    public User() {
    }

    public User(Integer pkId) {
        this.pkId = pkId;
    }

    public Integer getPkId() {
        return pkId;
    }

    public void setPkId(Integer pkId) {
        this.pkId = pkId;
    }

  

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

  
    
    public void setPassword(String password){
        this.password = password;
    }
    
    public String getPassword(){
        return password;
    }
    
    public void setSalt(String salt){
        this.salt = salt;
    }
    
    public String getSalt(){
        
        return salt;
    }

    @XmlTransient
    public Collection<CustomerOrder> getCustomerOrderCollection() {
        return customerOrderCollection;
    }

    public void setCustomerOrderCollection(Collection<CustomerOrder> customerOrderCollection) {
        this.customerOrderCollection = customerOrderCollection;
    }
    
    public CustomerOrder getOrderFromRestaurant(Restaurant restaurant) {
        for(CustomerOrder c : this.getCustomerOrderCollection()) {
            if(c.getFKRestaurant().getPkId().equals(restaurant.getPkId())) return c;
        }
        
        return null;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pkId != null ? pkId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof User)) {
            return false;
        }
        User other = (User) object;
        if ((this.pkId == null && other.pkId != null) || (this.pkId != null && !this.pkId.equals(other.pkId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.User[ pkId=" + pkId + " ]";
    }
    
    public static String password(String password, String salt) throws UnsupportedEncodingException, NoSuchAlgorithmException
    {
       
       String hash = password+salt;
       MessageDigest md = MessageDigest.getInstance("MD5");
       md.update(hash.getBytes());
       return (new HexBinaryAdapter()).marshal(md.digest());
    }
    
    public static String characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public static Random random = new Random();
    
    public static String salt()
    {
        String str ="" ;
		for(int i=0;i<20;i++)
		{
			str+= characters.charAt(random.nextInt(characters.length()));
		}
		
		return str;
    }
}
