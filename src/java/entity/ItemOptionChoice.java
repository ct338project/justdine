/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Adrian
 */
@Entity
@Table(name = "ItemOptionChoice")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ItemOptionChoice.findAll", query = "SELECT i FROM ItemOptionChoice i"),
    @NamedQuery(name = "ItemOptionChoice.findByPkId", query = "SELECT i FROM ItemOptionChoice i WHERE i.pkId = :pkId"),
    @NamedQuery(name = "ItemOptionChoice.findByTitle", query = "SELECT i FROM ItemOptionChoice i WHERE i.title = :title"),
    @NamedQuery(name = "ItemOptionChoice.findByPrice", query = "SELECT i FROM ItemOptionChoice i WHERE i.price = :price")})
public class ItemOptionChoice implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "PK_ID")
    private Integer pkId;
    @Size(max = 255)
    @Column(name = "title")
    private String title;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "price")
    private Double price;
    @JoinColumn(name = "FK_ItemOption", referencedColumnName = "PK_ID")
    @ManyToOne(optional = false)
    private ItemOption fKItemOption;

    public ItemOptionChoice() {
    }

    public ItemOptionChoice(Integer pkId) {
        this.pkId = pkId;
    }

    public Integer getPkId() {
        return pkId;
    }

    public void setPkId(Integer pkId) {
        this.pkId = pkId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public ItemOption getFKItemOption() {
        return fKItemOption;
    }

    public void setFKItemOption(ItemOption fKItemOption) {
        this.fKItemOption = fKItemOption;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pkId != null ? pkId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ItemOptionChoice)) {
            return false;
        }
        ItemOptionChoice other = (ItemOptionChoice) object;
        if ((this.pkId == null && other.pkId != null) || (this.pkId != null && !this.pkId.equals(other.pkId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ItemOptionChoice[ pkId=" + pkId + " ]";
    }
    
}
