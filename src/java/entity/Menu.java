/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Adrian
 */
@Entity
@Table(name = "Menu")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Menu.findAll", query = "SELECT m FROM Menu m"),
    @NamedQuery(name = "Menu.findByPkId", query = "SELECT m FROM Menu m WHERE m.pkId = :pkId")})
public class Menu implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "PK_ID")
    private Integer pkId;
    @OneToOne(cascade = CascadeType.PERSIST, optional = false, fetch = FetchType.EAGER)
    private Restaurant fkRestaurant;
    @OneToMany(cascade = CascadeType.PERSIST, mappedBy = "fKMenu", fetch = FetchType.EAGER)
    private Collection<MenuCategory> menuCategoryCollection;

    public Menu() {
    }

    public Menu(Integer pkId) {
        this.pkId = pkId;
    }

    public Integer getPkId() {
        return pkId;
    }

    public void setPkId(Integer pkId) {
        this.pkId = pkId;
    }

    public Restaurant getFkRestaurant() {
        return fkRestaurant;
    }

    public void setFkRestaurant(Restaurant fkRestaurant) {
        this.fkRestaurant = fkRestaurant;
    }

    @XmlTransient
    public Collection<MenuCategory> getMenuCategoryCollection() {
        Collections.sort((List<MenuCategory>) menuCategoryCollection);
        return menuCategoryCollection;
    }

    public void setMenuCategoryCollection(Collection<MenuCategory> menuCategoryCollection) {
        this.menuCategoryCollection = menuCategoryCollection;
    }
    
    public void addCategory(MenuCategory category) {
        if(this.menuCategoryCollection == null) this.menuCategoryCollection = new ArrayList<MenuCategory>();
        category.setFKMenu(this);
        this.menuCategoryCollection.add(category);
    }
    
    public MenuCategory getMenuCategory(String categoryName) {
        return null;
    }
    
    public boolean hasItem(MenuItem item) {
        for(MenuCategory category : this.getMenuCategoryCollection()) {
            if(category.hasItem(item)) return true;
        }
        
        return false;
    }
    
    public boolean hasCategory(MenuCategory category) {
        Collection<MenuCategory> categories = getMenuCategoryCollection();
        
        for(MenuCategory uCategory : categories) {
            if(uCategory.getPkId().equals(category.getPkId())) return true;
        }
        
        return false;
    }
    
    public void addMenuItem(MenuCategory category, MenuItem item) {
        
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pkId != null ? pkId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Menu)) {
            return false;
        }
        Menu other = (Menu) object;
        if ((this.pkId == null && other.pkId != null) || (this.pkId != null && !this.pkId.equals(other.pkId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Menu[ pkId=" + pkId + " ]";
    }
    
}
