/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Adrian
 */
@Entity
@Table(name = "ItemOption")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ItemOption.findAll", query = "SELECT i FROM ItemOption i"),
    @NamedQuery(name = "ItemOption.findById", query = "SELECT i FROM ItemOption i WHERE i.pkId = :pkId")
})
public class ItemOption implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "PK_ID")
    private Integer pkId;
    @Column(name = "Default_choice")
    private Integer default1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fKItemOption")
    private Collection<ItemOptionChoice> itemOptionChoiceCollection;
    @JoinColumn(name = "FK_MenuItem", referencedColumnName = "PK_ID")
    @ManyToOne(optional = false)
    private MenuItem fKMenuItem;

    public ItemOption() {
    }

    public ItemOption(Integer pkId) {
        this.pkId = pkId;
    }

    public Integer getPkId() {
        return pkId;
    }

    public void setPkId(Integer pkId) {
        this.pkId = pkId;
    }

    public Integer getDefault1() {
        return default1;
    }

    public void setDefault1(Integer default1) {
        this.default1 = default1;
    }

    @XmlTransient
    public Collection<ItemOptionChoice> getItemOptionChoiceCollection() {
        return itemOptionChoiceCollection;
    }

    public void setItemOptionChoiceCollection(Collection<ItemOptionChoice> itemOptionChoiceCollection) {
        this.itemOptionChoiceCollection = itemOptionChoiceCollection;
    }

    public MenuItem getFKMenuItem() {
        return fKMenuItem;
    }

    public void setFKMenuItem(MenuItem fKMenuItem) {
        this.fKMenuItem = fKMenuItem;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pkId != null ? pkId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ItemOption)) {
            return false;
        }
        ItemOption other = (ItemOption) object;
        if ((this.pkId == null && other.pkId != null) || (this.pkId != null && !this.pkId.equals(other.pkId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ItemOption[ pkId=" + pkId + " ]";
    }
    
}
