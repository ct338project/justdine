/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Comparator;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Adrian
 */
@Entity
@Table(name = "MenuCategory")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MenuCategory.findAll", query = "SELECT m FROM MenuCategory m"),
    @NamedQuery(name = "MenuCategory.findByPkId", query = "SELECT m FROM MenuCategory m WHERE m.pkId = :pkId"),
    @NamedQuery(name = "MenuCategory.findByTitle", query = "SELECT m FROM MenuCategory m WHERE m.title = :title"),
    @NamedQuery(name = "MenuCategory.findByDescription", query = "SELECT m FROM MenuCategory m WHERE m.description = :description")})
public class MenuCategory implements Serializable, Comparable<MenuCategory> {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "PK_ID")
    private Integer pkId;
    @Size(max = 255)
    @Column(name = "title")
    private String title;
    @Size(max = 255)
    @Column(name = "description")
    private String description;
    @Column(name = "categoryIndex")
    private Integer index = 0;
    @OneToMany(cascade = CascadeType.PERSIST, mappedBy = "fKMenuCategory", fetch = FetchType.EAGER)
    private Collection<MenuItem> menuItemCollection;
    @JoinColumn(name = "FK_Menu", referencedColumnName = "PK_ID")
    @ManyToOne(optional = false)
    private Menu fKMenu;

    public MenuCategory() {
    }
    
    public MenuCategory(String name, Integer index) {
        this.setTitle(name);
        this.setIndex(index);
    }

    public MenuCategory(Integer pkId) {
        this.pkId = pkId;
    }

    public Integer getPkId() {
        return pkId;
    }

    public void setPkId(Integer pkId) {
        this.pkId = pkId;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlTransient
    public Collection<MenuItem> getMenuItemCollection() {
        return menuItemCollection;
    }

    public void setMenuItemCollection(Collection<MenuItem> menuItemCollection) {
        this.menuItemCollection = menuItemCollection;
    }

    public Menu getFKMenu() {
        return fKMenu;
    }

    public void setFKMenu(Menu fKMenu) {
        this.fKMenu = fKMenu;
    }
    
    public boolean hasItem(MenuItem find) {
        for(MenuItem item : this.getMenuItemCollection()) {
            if(item.getPkId().equals(find.getPkId())) return true;
        }
        
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pkId != null ? pkId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MenuCategory)) {
            return false;
        }
        MenuCategory other = (MenuCategory) object;
        if ((this.pkId == null && other.pkId != null) || (this.pkId != null && !this.pkId.equals(other.pkId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.MenuCategory[ pkId=" + pkId + " ]";
    }

    @Override
    public int compareTo(MenuCategory o) {
        return this.getIndex() - o.getIndex();
    }
}
