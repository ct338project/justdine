/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Adrian
 */
@Entity
@Table(name = "MenuItem")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MenuItem.findAll", query = "SELECT m FROM MenuItem m"),
    @NamedQuery(name = "MenuItem.findByPkId", query = "SELECT m FROM MenuItem m WHERE m.pkId = :pkId"),
    @NamedQuery(name = "MenuItem.findByTitle", query = "SELECT m FROM MenuItem m WHERE m.title = :title"),
    @NamedQuery(name = "MenuItem.findByDescription", query = "SELECT m FROM MenuItem m WHERE m.description = :description"),
    @NamedQuery(name = "MenuItem.findByFlags", query = "SELECT m FROM MenuItem m WHERE m.flags = :flags"),
    @NamedQuery(name = "MenuItem.findByPrice", query = "SELECT m FROM MenuItem m WHERE m.price = :price")})
public class MenuItem implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "PK_ID")
    private Integer pkId;
    @Size(max = 255)
    private String title;
    @Size(max = 255)
    private String description;
    private Boolean flags;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    private Double price;
    @JoinColumn(name = "FK_MenuCategory", referencedColumnName = "PK_ID")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private MenuCategory fKMenuCategory;
    @OneToMany(cascade = CascadeType.PERSIST, mappedBy = "fKMenuItem")
    private Collection<ItemOption> itemOptionCollection;
    @ManyToMany(mappedBy = "menuItemCollection", cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    private Collection<CustomerOrder> customerOrderCollection;

    public MenuItem() {
    }

    public MenuItem(Integer pkId) {
        this.pkId = pkId;
    }

    public Integer getPkId() {
        return pkId;
    }

    public void setPkId(Integer pkId) {
        this.pkId = pkId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getFlags() {
        return flags;
    }

    public void setFlags(Boolean flags) {
        this.flags = flags;
    }

    public Double getPrice() {
        return price;
    }
    
    public String getFixedPrice() {
        return String.format("%.2f", this.getPrice());
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public MenuCategory getFKMenuCategory() {
        return fKMenuCategory;
    }

    public void setFKMenuCategory(MenuCategory fKMenuCategory) {
        this.fKMenuCategory = fKMenuCategory;
    }

    @XmlTransient
    public Collection<ItemOption> getItemOptionCollection() {
        return itemOptionCollection;
    }

    public void setItemOptionCollection(Collection<ItemOption> itemOptionCollection) {
        this.itemOptionCollection = itemOptionCollection;
    }

    @XmlTransient
    public Collection<CustomerOrder> getCustomerOrderCollection() {
        return customerOrderCollection;
    }

    public void setCustomerOrderCollection(Collection<CustomerOrder> customerOrderCollection) {
        this.customerOrderCollection = customerOrderCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pkId != null ? pkId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MenuItem)) {
            return false;
        }
        MenuItem other = (MenuItem) object;
        if ((this.pkId == null && other.pkId != null) || (this.pkId != null && !this.pkId.equals(other.pkId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.MenuItem[ pkId=" + pkId + " ]";
    }
    
}
