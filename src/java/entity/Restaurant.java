/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import servlet.Controller;

/**
 *
 * @author Adrian
 */
@Entity
@Table(name = "Restaurant")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Restaurant.findAll", query = "SELECT r FROM Restaurant r"),
    @NamedQuery(name = "Restaurant.findByPkId", query = "SELECT r FROM Restaurant r WHERE r.pkId = :pkId")
})
public class Restaurant implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "PK_ID")
    private Integer pkId;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "rating", columnDefinition="INT")
    private int rating;
    @Column(name = "category")
    private String category;
    @Column(name = "photo")
    private String photo;
    @JoinColumn(name = "FK_Owner", referencedColumnName = "PK_ID")
    @ManyToOne(optional = false)
    private User fKOwner;
    @JoinColumn(name = "FK_Menu", referencedColumnName = "PK_ID")
    @OneToOne(cascade = CascadeType.PERSIST, optional = false, fetch = FetchType.EAGER)
    private Menu fKMenu;
    @OneToMany(cascade = CascadeType.PERSIST, mappedBy = "fKRestaurant")
    private Collection<CustomerOrder> customerOrderCollection;

    public Restaurant() {
    }

    public Restaurant(Integer pkId) {
        this.pkId = pkId;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public Menu getMenu() {
        return fKMenu;
    }

    public void setMenu(Menu fKMenu) {
        this.fKMenu = fKMenu;
    }

    public User getOwner() {
        return fKOwner;
    }

    public void setOwner(User fKOwner) {
        this.fKOwner = fKOwner;
    }

    public Integer getPkId() {
        return pkId;
    }

    public void setPkId(Integer pkId) {
        this.pkId = pkId;
    }
    
    public boolean isOwner(User user) {
        return this.getOwner().getPkId().equals(user.getPkId());
    }
    
    @XmlTransient
    public Collection<CustomerOrder> getCustomerOrderCollection() {
        return customerOrderCollection;
    }

    public void setCustomerOrderCollection(Collection<CustomerOrder> customerOrderCollection) {
        this.customerOrderCollection = customerOrderCollection;
    }
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pkId != null ? pkId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Restaurant)) {
            return false;
        }
        Restaurant other = (Restaurant) object;
        if ((this.pkId == null && other.pkId != null) || (this.pkId != null && !this.pkId.equals(other.pkId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Restaurant[ pkId=" + pkId + " ]";
    }
}
