/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Adrian
 */
@Entity
@Table(name = "CustomerOrder")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CustomerOrder.findAll", query = "SELECT c FROM CustomerOrder c"),
    @NamedQuery(name = "CustomerOrder.findByPkId", query = "SELECT c FROM CustomerOrder c WHERE c.pkId = :pkId"),
    @NamedQuery(name = "CustomerOrder.findByStatus", query = "SELECT c FROM CustomerOrder c WHERE c.status = :status")})
public class CustomerOrder implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "PK_ID")
    private Integer pkId;
    @Column(name = "status")
    private Integer status;
    @ManyToMany(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
    private Collection<MenuItem> menuItemCollection;
    @JoinColumn(name = "FK_Restaurant", referencedColumnName = "PK_ID")
    @ManyToOne(cascade = CascadeType.REFRESH, optional = false, fetch = FetchType.EAGER)
    private Restaurant fKRestaurant;
    @JoinColumn(name = "FK_User", referencedColumnName = "PK_ID")
    @ManyToOne(cascade = CascadeType.REFRESH, optional = false, fetch = FetchType.EAGER)
    private User fKUser;
    @Column(name = "date_added", nullable = false, updatable = false, insertable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Timestamp dateAdded;

    public CustomerOrder() {
    }

    public CustomerOrder(Integer pkId) {
        this.pkId = pkId;
    }

    public Integer getPkId() {
        return pkId;
    }

    public void setPkId(Integer pkId) {
        this.pkId = pkId;
    }

    public Timestamp getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(Timestamp dateAdded) {
        this.dateAdded = dateAdded;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Restaurant getFKRestaurant() {
        return fKRestaurant;
    }

    public void setFKRestaurant(Restaurant fKRestaurant) {
        this.fKRestaurant = fKRestaurant;
    }

    public User getFKUser() {
        return fKUser;
    }

    public void setFKUser(User fKUser) {
        this.fKUser = fKUser;
    }

    public Collection<MenuItem> getMenuItemCollection() {
        return menuItemCollection;
    }

    public void setMenuItemCollection(Collection<MenuItem> menuItemCollection) {
        this.menuItemCollection = menuItemCollection;
    }
    
    public void addItem(MenuItem item) {
        Collection<MenuItem> items = this.getMenuItemCollection();
        if(items == null) {
            items = new ArrayList<>();
            this.setMenuItemCollection(items);
        }
        
        items.add(item);
    }
    
    public boolean hasItem(MenuItem item) {
        for(MenuItem i : this.getMenuItemCollection()) {
            if(item.getPkId().equals(i.getPkId())) return true;
        }
        
        return false;
    }
    
    public double getOrderTotal() {
        double total = 0;
        for(MenuItem item : this.getMenuItemCollection()) {
            total += item.getPrice();
        }
        
        return total;
    }
    
    public int getItemCount() {
        return this.getMenuItemCollection().size();
    }
    
    public String getFixedOrderTotal() {
        return String.format("%.2f", this.getOrderTotal());
    }
    
    public String getDateAddedString() {
        return new SimpleDateFormat("MM/dd/yyyy").format(getDateAdded());
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pkId != null ? pkId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CustomerOrder)) {
            return false;
        }
        CustomerOrder other = (CustomerOrder) object;
        if ((this.pkId == null && other.pkId != null) || (this.pkId != null && !this.pkId.equals(other.pkId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CustomerOrder[ pkId=" + pkId + " ]";
    }
    
}
