/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.CustomerOrder;
import entity.Restaurant;
import entity.User;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Adrian
 */
@Stateless
public class CustomerOrderFacade extends AbstractFacade<CustomerOrder> {
    @PersistenceContext(unitName = "web-jpaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CustomerOrderFacade() {
        super(CustomerOrder.class);
    }
    
    public CustomerOrder getCustomerOrderForRestaurant(User user, Restaurant restaurant) {
        Query query = em.createQuery("SELECT co FROM CustomerOrder co WHERE co.fKUser = :user AND co.fKRestaurant = :restaurant", CustomerOrder.class);
        query.setParameter("user", user);
        query.setParameter("restaurant", restaurant);
        
        try {
            return (CustomerOrder) query.getSingleResult();
        } catch(NoResultException e) {
            return null;
        }
    }
    
    public CustomerOrder createCustomerOrderForRestaurant(User user, Restaurant restaurant) {
        CustomerOrder order = new CustomerOrder();
        order.setFKUser(user);
        order.setFKRestaurant(restaurant);
        return order;
    }
}
