/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.CustomerOrder;
import entity.Restaurant;
import entity.User;
import servlet.Controller;
import servlet.exception.HttpException;
import servlet.exception.IncorrectCredentialsException;
import servlet.exception.InternalException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Sean
 */
@Stateless
public class UserFacade extends AbstractFacade<User> {

    @PersistenceContext(unitName = "web-jpaPU")
    private EntityManager em;
    private User user;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UserFacade() {
        super(User.class);
    }

    /**
     * Login a user and return the logged in user.
     * @param email
     * @param password
     * @return
     * @throws UnsupportedEncodingException
     * @throws NoSuchAlgorithmException 
     */
 
    public User login(String email, String password) throws InternalException {

        try {
            User results = (User) em.createNamedQuery("User.findByEmail")
                .setParameter("email", email).getSingleResult();

            String salt = results.getSalt();
            String hash = User.password(password, salt);

            if ((results.getPassword().equals(hash))) {
                return results;
            } else {
                return null;
            }
        } catch(NoResultException e) {
            return null;
        } catch(UnsupportedEncodingException | NoSuchAlgorithmException e) {
            throw new InternalException();
        }
    }
       
    public User findUser(String email) throws InternalException{
        
        try{
            
            User results = (User) em.createNamedQuery("User.findByEmail")
                .setParameter("email", email).getSingleResult();
            
            if(results.getEmail().equals(email))
            {
                return results;
            }
            else{return null;}
            
        }
        catch(NoResultException e) {
            return null;
        }
        
    }
    /**
     * Log in a user and set their session variable.
     * @param request
     * @param email
     * @param password
     * @throws UnsupportedEncodingException
     * @throws NoSuchAlgorithmException 
     */
    public void login(HttpServletRequest request, String email, String password) throws InternalException, IncorrectCredentialsException {
        User user = login(email, password);
        
        if(user != null) request.getSession().setAttribute("user", user);
        else throw new IncorrectCredentialsException();
    }
    
    /**
     * Log a user out of the current session.
     * @param request 
     */
    public void logout(HttpServletRequest request) {
        request.getSession().setAttribute("user", null);
    }
}
