# Just Dine Source Code
### CT338 Project by Adrian Cooney, Sean Collum, Niall Martin, Shane O'Rourke

### Installation
So we spent a lot of time ensuring the project worked on all computers via git, this mean't that all resource, database schemas and data should be automatically generated without the need for any manual tweaking. This was hard and worked MOST of the time. Usually there was some error but if you repeated the process, it magically disappeared. The source code lives on a git repository hosted on Bitbucket at https://bitbucket.org/ct338project/justdine. These are the steps taken to get the project up an running and should if everything goes well, get the site up and running:

#### Prerequisites
To automatically insert the data into the database, we used Node.js and emulated browser requests (long story). These aren't necessary if you have an database export but is necessary step in the build without it. Otherwise, you'll have to manually create a user, restaurant and menu (which are all possible from the website interface, but tedious).

1. Download [http://nodejs.org](http://nodejs.org) and install.
2. Navigate to the JustDine directory.
3. `cd setup/generate` and run `npm install`. This will install all the dependancies to run the generator.

#### Cloning
1. Open Netbeans
2. Team > Git > Clone [https://bitbucket.org/ct338project/justdine.git](https://bitbucket.org/ct338project/justdine.git).
3. Clone project.

#### Running
1. Clean and build.
2. Run the project, hope no errors arise and go to http://localhost:8080/JustDine/.
3. `cd JustDine/setup/generate` and run `npm run generate` OR import the database export.

#### Troubleshooting
1. JUnit plugin may not be installed on Netbeans. We did start creating test but it slowed down the process tenfold, and now we can't seem to get rid of it from the build process. Installing JUnit will fix the problem.
2. The database may not be configured correctly with the remote. Please scan through `persistance.xml` and ensure the (1) if the data source if localhost, you have MySql installed and a database called `ct338project` (2) if the data source is `danu6`, the connection being made successfully.
3. The SASS binary needs to be included to compile the CSS (if you make any changes, a built one should be present). Go to Project Properties > CSS Preprocessors > Configure Executables > Install SASS.

### Usage
You can navigate to [http://localhost:8080/JustDine/login](http://localhost:8080/JustDine/login) to login with the default user credentials: username `root` and password `root`.

### Project
Here you find a full git history with detailed commit logs of all changes, tagged releases and clean, branched history.

### Patterns
#### Routing
Some patterns emerged when we designed out servlets. Since Glassfish didn't play well to multi level paths to servlets (e.g. /restaurant/new) we resorted to using the GET parameters within the URL to differentiate between pages. One of the methods in the handy library we created in `Controller.java` was `page`. We used this to switch over the pages. For example, if the servlet had this as it's GET method's body, handle requests on the `/restaurant` path and we hit the server with `/restaurant?page=menu`, the "menu" clause in the switch would be hit.

	switch(Controller.page(request)) {
		case "menu":
			// Create a new restaurant
		break;
	}

Alongside the `page` parameter, the `page` method also handled the `action` GET parameter to allow the servlet to add another degree of specifity to it's URL scheme. For example, if we wanted to create a new order on the restaurant, you would hit the url `/restaurant?page=order&action=new` (which is exactly what [we did](https://bitbucket.org/ct338project/justdine/src/35658348d4345c67f97482139e2d924bda5d3781/src/java/servlet/RestaurantServlet.java?at=master#cl-111)):

	switch(Controller.page(request)) {
		case "order:new":
			// Create a new restaurant.
		break;
	}

#### Exceptions
Another pattern we used heavily was custom exception throwing. For each possible error event that could happen from forgetting a parameter in a form to someone attempting to access somewhere they shouldn't, we created exceptions (see `servet.exception.*`). These all inherited from our custom `HttpException` which could then be passed onto the `Controller#fail` and it would appropriately handle the response given the exception. For example, a snippet we used quite often to ensure the right parameters were passed to the servlet was `Controller#hasRequired`. You passed in all the required parameters, for example a POST request `/login` needed `username` and `password`. If they weren't present, a `MissingParamException` was thrown with the correct HTTP response code and message alongside a custom message. This exception would then be passed onto the `Controller#fail` which would display it to the user in a nice error page and give them options on how to fix their error. This example doesn't really show the power of the `HttpException` pattern until you throw more functions into the mix such as `Controller#isLoggedIn` or throwing your own `HttpException`. An actual code example is worth a thousand words:

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    try {
	    	switch(Controller.page(request)) {
	    		case "order:new"
	    			// Make sure the user is logged in and has passed the required parameters.
		            if(Controller.isLoggedIn(request) && Controller.hasRequired(new String[] { "name", "contents" })) {
		                // Create new order..
		            }
		        break;

		        default:
		        	throw new HttpException(404); // 404, Not found
	        }
        } catch(UnauthorizedException ex) {
        	// Catch an individual exception, out of the way of the main body to keep code clean and organized
        	// and do custom 
			Controller.redirect(response, "/login");
        } catch (HttpException ex) {
        	// Consolodate all the other exceptions to one error point
        	// Which will handle the exception in a standard, consist
        	// manner.
            Controller.fail(request, response, ex);
        }
    }

For some more examples of the `HttpException` pattern in use, see [here](https://bitbucket.org/ct338project/justdine/src/35658348d4345c67f97482139e2d924bda5d3781/src/java/servlet/RegistrationServlet.java?at=master#cl-45), [here](https://bitbucket.org/ct338project/justdine/src/35658348d4345c67f97482139e2d924bda5d3781/src/java/servlet/MenuServlet.java?at=master#cl-53) and [here](https://bitbucket.org/ct338project/justdine/src/35658348d4345c67f97482139e2d924bda5d3781/src/java/servlet/MenuServlet.java?at=master#cl-158).
