ALTER TABLE Restaurant DROP FOREIGN KEY `has a`;
ALTER TABLE ItemOption DROP FOREIGN KEY `has many`;
ALTER TABLE `Order` DROP FOREIGN KEY `has many2`;
ALTER TABLE `Order` DROP FOREIGN KEY `has many3`;
ALTER TABLE MenuItem DROP FOREIGN KEY `has many4`;
ALTER TABLE MenuCategory DROP FOREIGN KEY FKMenuCatego627674;
ALTER TABLE ItemOptionChoice DROP FOREIGN KEY `has mant`;
ALTER TABLE `Order` DROP FOREIGN KEY `has many5`;

DROP TABLE IF EXISTS Menu;
DROP TABLE IF EXISTS `User`;
DROP TABLE IF EXISTS MenuItem;
DROP TABLE IF EXISTS MenuCategory;
DROP TABLE IF EXISTS ItemOption;
DROP TABLE IF EXISTS Restaurant;
DROP TABLE IF EXISTS `Order`;
DROP TABLE IF EXISTS ItemOptionChoice;

CREATE TABLE Menu (
  PK_ID int(10) NOT NULL AUTO_INCREMENT, 
  PRIMARY KEY (PK_ID));

CREATE TABLE `User` (
  PK_ID       int(10) NOT NULL AUTO_INCREMENT, 
  Name        varchar(255), 
  Email       varchar(255),
  PRIMARY KEY (PK_ID));

CREATE TABLE MenuItem (
  PK_ID           int(10) NOT NULL AUTO_INCREMENT, 
  FK_MenuCategory int(10) NOT NULL, 
  Title           varchar(255), 
  Description     varchar(255), 
  Flags           tinyint(1), 
  Price           double, 
  PRIMARY KEY (PK_ID));

CREATE TABLE MenuCategory (
  PK_ID       int(10) NOT NULL AUTO_INCREMENT, 
  FK_Menu     int(10) NOT NULL, 
  Title       varchar(255), 
  Description varchar(255), 
  PRIMARY KEY (PK_ID));

CREATE TABLE ItemOption (
  PK_ID       int(10) NOT NULL AUTO_INCREMENT, 
  FK_MenuItem int(10) NOT NULL, 
  `Default`   int(10), 
  PRIMARY KEY (PK_ID));

CREATE TABLE Restaurant (
  PK_ID   int(10) NOT NULL AUTO_INCREMENT, 
  FK_Menu int(10) NOT NULL, 
  Photos  int(10), 
  PRIMARY KEY (PK_ID));

CREATE TABLE `Order` (
  PK_ID         int(10) NOT NULL AUTO_INCREMENT, 
  FK_MenuItem   int(10), 
  FK_Restaurant int(10) NOT NULL, 
  FK_User       int(10) NOT NULL, 
  Status        int(10), 
  PRIMARY KEY (PK_ID));

CREATE TABLE ItemOptionChoice (
  PK_ID         int(10) NOT NULL AUTO_INCREMENT, 
  FK_ItemOption int(10) NOT NULL, 
  Title         varchar(255), 
  Price         double, 
  PRIMARY KEY (PK_ID));

ALTER TABLE Restaurant ADD INDEX `has a` (FK_Menu), ADD CONSTRAINT `has a` FOREIGN KEY (FK_Menu) REFERENCES Menu (PK_ID);
ALTER TABLE ItemOption ADD INDEX `has many` (FK_MenuItem), ADD CONSTRAINT `has many` FOREIGN KEY (FK_MenuItem) REFERENCES MenuItem (PK_ID);
ALTER TABLE `Order` ADD INDEX `has many2` (FK_User), ADD CONSTRAINT `has many2` FOREIGN KEY (FK_User) REFERENCES `User` (PK_ID);
ALTER TABLE `Order` ADD INDEX `has many3` (FK_Restaurant), ADD CONSTRAINT `has many3` FOREIGN KEY (FK_Restaurant) REFERENCES Restaurant (PK_ID);
ALTER TABLE MenuItem ADD INDEX `has many4` (FK_MenuCategory), ADD CONSTRAINT `has many4` FOREIGN KEY (FK_MenuCategory) REFERENCES MenuCategory (PK_ID);
ALTER TABLE MenuCategory ADD INDEX FKMenuCatego627674 (FK_Menu), ADD CONSTRAINT FKMenuCatego627674 FOREIGN KEY (FK_Menu) REFERENCES Menu (PK_ID);
ALTER TABLE ItemOptionChoice ADD INDEX `has mant` (FK_ItemOption), ADD CONSTRAINT `has mant` FOREIGN KEY (FK_ItemOption) REFERENCES ItemOption (PK_ID);
ALTER TABLE `Order` ADD INDEX `has many5` (FK_MenuItem), ADD CONSTRAINT `has many5` FOREIGN KEY (FK_MenuItem) REFERENCES MenuItem (PK_ID);
