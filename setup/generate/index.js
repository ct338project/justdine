var faker = require("faker"),
	request = require("request").defaults({ jar: true }),
	async = require("async"),

	data = require("./data");

require("request-debug")(request);

/*
 * So after MUCH FRUSTRATION WITH JPA (I DON'T USE UPPERCASE CHARACTERS LIGHTLY),
 * I've resorted to manually creating the data using Node.js because I can't seem
 * to create a TINY SIMPLY FUCKING SCRIPT to create the fake data. I mean, after
 * nearly two hours of trying to run a simple class with a main function to
 * access the entity manager, you reach a point where you question why you bother
 * at all. It's seems EVERY thing is a struggle with JavaEE and it's ridiculous 
 * that it's entry level architecture for web development.
 *
 * So here, I've resorted to emulating the requests from the HTML form to create
 * the users via POST.
 * 
 * WHY CANT JAVA DO ANYTHING EASILY I MEAN REALLY. FUCK ANT. FUCK CLASS PATHS. 
 */
const HOST = "http://localhost:8080/JustDine"

/**
 * Steps this file takes.
 *
 * 1. Creates the admin user.
 * 2. Creates 10 restaurants that admin owns.
 */
async.series([
	function(callback) {
		console.log("-> Creating admin user.");
		post("/registration", {
			name: "Admin",
			email: "root",
			password: "root",
			confirmpassword: "root"
		}, callback);
	},

	function(callback) {
		console.log("-> Generating restaurant");
		async.eachSeries(data.restaurants, post.bind(null, "/restaurant?page=new"), callback);
	},

	function(callback) {
		console.log("-> Generate sample menu");
		post("/menu?page=item&action=create&restaurant=1&category=1", {
			title: "Soup of the day",
			subtitle: "Served with minted brown bread and butter.",
			price: "8.00"
		}, callback);
	},

	function(callback) {
		post("/menu?page=item&action=create&restaurant=1&category=2", {
			title: "Full rack of BBQ ribs",
			subtitle: "Slow cooked for 12 hours, ready to serve.",
			price: "18.00"
		}, callback);
	},

	function(callback) {
		post("/menu?page=item&action=create&restaurant=1&category=3", {
			title: "Ice Cream",
			subtitle: "3 scoops of delicious Ben & Jerrys",
			price: "5.00"
		}, callback);
	}
], function(err) {
	if(err) console.log("Data generation failed.", err);
	else console.log("Data generation complete.");
})


function post(path, data, callback) {
	request({
		url: HOST + path,
		form: data || {},
		method: "POST"
	}, callback);
}