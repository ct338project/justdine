<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="justdine" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 

<justdine:page pageTitle="Login">
    <jsp:attribute name="pageContent">
        <div class="login">
            <justdine:errorMessage error="${error}"></justdine:errorMessage>
            <form action="" method="POST">
                <div class="input icon-mail">
                    <label>Email</label>
                    <input type="text" name="email" placeholder="Email">
                </div>

                <div class="input icon-fingerprint">
                    <label>Password</label>
                    <input type="password" name="password" placeholder="Password">
                </div>
                
                <div class="input submit">
                    <input type="submit" value="Login">
                </div>
            </form>
        </div>
    </jsp:attribute>
</justdine:page>