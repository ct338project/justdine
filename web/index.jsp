<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="justdine" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 

<justdine:page pageTitle="Just Dine">
    <jsp:attribute name="pageContent">
        <div class="static-webpage">
            <h1>Welcome!</h1>
            
            <p>
                Welcome to Just Dine, our project for CT338 Software Engineering. Please use the links above to navigate through the website.
            </p>
        </div>
    </jsp:attribute>
</justdine:page>
