<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<%@taglib prefix="justdine" tagdir="/WEB-INF/tags" %>

<justdine:page pageTitle="Sign up">
    <jsp:attribute name="pageContent">
    <div class="grid signup">
        <form class="cell" id="userRegistration" action="" method="post">
            <justdine:errorMessage error="${ error }"></justdine:errorMessage>
            <div class="input icon-person">
                <label>Name</label>
                <input type="text" name="name" placeholder="Name">
            </div>

            <div class="input icon-mail">
                <label>Email</label>
                <input type="text" name="email" placeholder="Email">
            </div>

            <div class="input icon-fingerprint">
                <label>Password</label>
                <input type="password" name="password" placeholder="Password">
            </div>
            
            <div class="input icon-fingerprint">
                <label>Confirm Password</label>
                <input type="password" name="confirmpassword" placeholder="Confirm password">
            </div>
            

            <div class="input submit">
                <input type="submit" value="Submit" />
            </div>
        </form>
         <div class="signup-info">
             <p>Sign up for a new JustDine account to get start rating, reviewing and dining at your favorite restaurants right away. No more waiting for your food.</p>
         </div>
    </div>
    </jsp:attribute>
</justdine:page>
