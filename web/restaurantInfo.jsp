<%-- 
    Document   : restaurantMenu
    Created on : Jan 29, 2015, 6:13:34 PM
    Author     : Adrian
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="justdine" tagdir="/WEB-INF/tags" %>

<justdine:restaurantPage restaurant="${restaurant}" pageName="info" pageTitle="Place Info">
    <jsp:attribute name="pageContent">
    <div class="info-block grid">
				<div class="cell photo">
					<img src="" alt="">
				</div>
				<div class="cell">
					<ul>
						<li><img src="assets/icon/phone-icon.svg"> 049 855 3641</li>
						<li><img src="assets/icon/mail-icon.svg"><a href="#">footandmouth@ni.ie</a></li>
						<li><img src="assets/icon/link-icon.svg"><a href="#">http://footandmouth.ni.ie</a></li>
					</ul>
					<div class="map-tile">
						
					</div>
				</div>
			</div>
			<h4 class="sub-title"><span>Reviews</span></h4>
			<div class="reviews">
				<div class="review">
					<div class="review-title">
						<img class="avatar" src="" alt="">
						<h3>Incredible Service, top quality spot.</h3>
						<h4><span class="rating"><strong>&#9733; &#9733; &#9733;</strong> &#9733; &#9733;</span> By Adrian Cooney on the 19/12/2014.</h4>
					</div>
					<div class="review-content grid">
						<div class="cell">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptate quos sequi omnis, sunt! Vitae, doloremque, assumenda! Accusamus mollitia minus, ipsa, vitae suscipit atque quisquam cumque dicta, architecto placeat, necessitatibus voluptatibus.</p>
						</div>
						<div class="ratings">
							<ul>
								<li>Food <span class="rating"><strong>&#9733; &#9733; &#9733;</strong> &#9733; &#9733;</span></li>
								<li>Service <span class="rating"><strong>&#9733; &#9733; &#9733;</strong> &#9733; &#9733;</span></li>
								<li>Speed <span class="rating"><strong>&#9733; &#9733; &#9733;</strong> &#9733; &#9733;</span></li>
								<li>Location <span class="rating"><strong>&#9733; &#9733; &#9733;</strong> &#9733; &#9733;</span></li>
								<li class="overall">Overall <span class="rating"><strong>&#9733; &#9733; &#9733;</strong> &#9733; &#9733;</span></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="review">
					<div class="review-title">
						<img class="avatar" src="" alt="">
						<h3>Incredible Service, top quality spot.</h3>
						<h4><span class="rating"><strong>&#9733; &#9733; &#9733;</strong> &#9733; &#9733;</span> By Adrian Cooney on the 19/12/2014.</h4>
					</div>
					<div class="review-content grid">
						<div class="cell">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptate quos sequi omnis, sunt! Vitae, doloremque, assumenda! Accusamus mollitia minus, ipsa, vitae suscipit atque quisquam cumque dicta, architecto placeat, necessitatibus voluptatibus.</p>
						</div>
						<div class="ratings">
							<ul>
								<li>Food <span class="rating"><strong>&#9733; &#9733; &#9733;</strong> &#9733; &#9733;</span></li>
								<li>Service <span class="rating"><strong>&#9733; &#9733; &#9733;</strong> &#9733; &#9733;</span></li>
								<li>Speed <span class="rating"><strong>&#9733; &#9733; &#9733;</strong> &#9733; &#9733;</span></li>
								<li>Location <span class="rating"><strong>&#9733; &#9733; &#9733;</strong> &#9733; &#9733;</span></li>
								<li class="overall">Overall <span class="rating"><strong>&#9733; &#9733; &#9733;</strong> &#9733; &#9733;</span></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="review">
					<div class="review-title">
						<img class="avatar" src="" alt="">
						<h3>Incredible Service, top quality spot.</h3>
						<h4><span class="rating"><strong>&#9733; &#9733; &#9733;</strong> &#9733; &#9733;</span> By Adrian Cooney on the 19/12/2014.</h4>
					</div>
					<div class="review-content grid">
						<div class="cell">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptate quos sequi omnis, sunt! Vitae, doloremque, assumenda! Accusamus mollitia minus, ipsa, vitae suscipit atque quisquam cumque dicta, architecto placeat, necessitatibus voluptatibus.</p>
						</div>
						<div class="ratings">
							<ul>
								<li>Food <span class="rating"><strong>&#9733; &#9733; &#9733;</strong> &#9733; &#9733;</span></li>
								<li>Service <span class="rating"><strong>&#9733; &#9733; &#9733;</strong> &#9733; &#9733;</span></li>
								<li>Speed <span class="rating"><strong>&#9733; &#9733; &#9733;</strong> &#9733; &#9733;</span></li>
								<li>Location <span class="rating"><strong>&#9733; &#9733; &#9733;</strong> &#9733; &#9733;</span></li>
								<li class="overall">Overall <span class="rating"><strong>&#9733; &#9733; &#9733;</strong> &#9733; &#9733;</span></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
    </jsp:attribute>
</justdine:restaurantPage>