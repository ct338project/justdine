<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<%@ taglib prefix="justdine" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 

<justdine:page pageTitle="New Restaurant">
    <jsp:attribute name="pageContent">
    <div class="grid signup">
        <form class="cell" id="userRegistration" action="" method="post">
            <justdine:errorMessage error="${error}"></justdine:errorMessage>
            <input type="hidden" name="page" value="new" />
            
            <div class="input icon-person">
                <label>Name</label>
                <input type="text" name="name" placeholder="Name">
            </div>

            <div class="input icon-pencil">
                <label>Description</label>
                <textarea name="description"></textarea>
            </div>

            <div class="input icon-pencil">
                <label>Category</label>
                <input type="text" name="category" placeholder="Category">
            </div>

            <div class="input submit">
                <input type="submit" value="Submit" />
            </div>
        </form>
         <div class="signup-info">
             <p class="need-you"><img src="/JustDine/assets/image/You.jpeg" /></p>
             <p>Add your own restaurant to our list. It's great really, you'll get tons of more business! Tons!</p>
        </div>
    </div>
    </jsp:attribute>
</justdine:page>