<%-- 
    Document   : restaurantMenu
    Created on : Jan 29, 2015, 6:13:34 PM
    Author     : Adrian
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="justdine" tagdir="/WEB-INF/tags" %>

<justdine:restaurantPage restaurant="${restaurant}" pageName="order" pageTitle="Place Order">
    <jsp:attribute name="pageContent">
        <div class="grid">
            <div class="cell order-content">
               <h4 class="sub-title"><span>Order</span></h4>
               <justdine:orderList order="${ order }" full="${ true }"></justdine:orderList>
            </div>
            <div class="cell">
                <div class="reservation">
                    <h4 class="sub-title"><span>Reservation</span></h4>
                    <div class="input icon-pencil">
                        <label>Name</label>
                        <input type="text" <c:if test="${ user != null}">value="${ user.getName() }"</c:if>>
                    </div>

                    <div class="grid">
                        <div class="cell input icon-clock">
                            <label>Arrival Time</label>
                            <input type="text">
                        </div>
                        <div class="gutter-10px"></div>
                        <div class="cell input icon-person">
                            <label>Table Size</label>
                            <input type="text">
                        </div>
                    </div>
                </div>
                <div class="payment">
                    <h4 class="sub-title"><span>Payment</span></h4>
                    <div class="input icon-pencil">
                        <label>Name</label>
                        <input type="text" <c:if test="${ user != null}">value="${ user.getName() }"</c:if>>
                    </div>

                    <div class="input icon-card">
                        <label>Card Number</label>
                        <input type="text">
                    </div>

                    <div class="grid">
                        <div class="cell input icon-calendar">
                            <label>Expiration</label>
                            <input type="text">
                        </div>
                        <div class="gutter-10px"></div>
                        <div class="cell input icon-lock">
                            <label>Security Code</label>
                            <input type="text">
                        </div>
                    </div>
                    
                    <div class="input submit">
                        <input type="submit" value="Submit" disabled>
                    </div>
                </div>
            </div>
        </div>
    </jsp:attribute>
</justdine:restaurantPage>