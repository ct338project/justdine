<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="justdine" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 

<justdine:page pageTitle="About">
    <jsp:attribute name="pageContent">
        <div class="static-webpage">
            <p>
                Just Dine originally started out as a software engineering project created by Sean Collum, Adrian Cooney, 
                Niall Martin and Shane O'Rourke. After receiving spectacular grades on the assignment, the group decided
                to expand upon the project and make it commercially available to the public.
            </p>
            <p>
                After launching the service in Ireland in June 2014, the website became an instant hit, and after six months it beat Google
                to become the most visited website on the internet in Ireland. With this success and publicity, the group were able to expand their
                business to a global scale and were enjoying revenue of millions.        
            </p>
            <p>
                After two years of revolutionising the online restaurant ordering service landscape, the group recently accepted a bid from Google
                and sold the company for a total of five hundred billion dollars, and are now enjoying retirement at the ripe old age of 23 years old.
            </p>
        </div>
    </jsp:attribute>
</justdine:page>
