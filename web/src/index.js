var page = require("page"),

	// Require the routes
	routes = require("./routes");

// The base path
var BASE_PATH = "/JustDine";

// Router
page({
	click: false,
	popstate: false
});

page(BASE_PATH + "/registration", routes.registration);
page();