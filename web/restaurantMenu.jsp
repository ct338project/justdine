<%-- 
    Document   : restaurantMenu
    Created on : Jan 29, 2015, 6:13:34 PM
    Author     : Adrian
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="justdine" tagdir="/WEB-INF/tags" %>

<justdine:restaurantPage restaurant="${restaurant}" pageName="menu" pageTitle="View Menu">
    <jsp:attribute name="pageContent">
        <div class="grid">
            <div class="menu-items">
                <c:forEach var="category" items="${ restaurant.getMenu().getMenuCategoryCollection() }">
                    <justdine:menuCategory restaurant="${ restaurant }" category="${category}"></justdine:menuCategory>
                </c:forEach>
            </div>
            <div class="menu-order cell">
                    <div class="order-header grid">
                            <h4 class="cell">Order</h4>
                            <!-- <h5 class="grid"><span class="cell">(1/4)</span> <button class="icon-button small add light"></button></h5>-->
                    </div>
                    <!--<div class="user-visor grid">
                            <div class="flap left">&#10096;</div>
                            <div class="user-name cell"><h5>Person 1</h5></div>
                            <div class="flap left">&#10097;</div>
                    </div>-->
                    <justdine:orderList order="${ order }"></justdine:orderList>
                            <!--<h6>Starter</h6>
                            <p>Breaded Garlic Mushrooms <a href="#" class="edit-order">Edit</a> <a href="#" class="edit-order">Remove</a></p>
                            <h6>Mains</h6>
                            <p>Dry Aged Rib Eye Steak <a href="#" class="edit-order">Edit</a> <a href="#" class="edit-order">Remove</a></p>-->
                            
            </div>
        </div>
    </jsp:attribute>
</justdine:restaurantPage>