<!--
  Copyright (c) 2010, Oracle. All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

  * Neither the name of Oracle nor the names of its contributors
    may be used to endorse or promote products derived from this software without
    specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
  THE POSSIBILITY OF SUCH DAMAGE.
-->


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="justdine" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 

<c:set var="user" value="${sessionScope.user}"></c:set>


<justdine:page pageTitle="Profile">
    <jsp:attribute name="pageContent">
        <h1>Hi there, ${ user.getName() }.</h1>
        <div class="grid">
            <div class="cell">
                <form id="userRegistration" action="" method="post">
                    <div class="input icon-person">
                        <label>Name</label>
                        <input type="text" name="name" value="${ user.getName() }">
                    </div>

                    <div class="input icon-mail">
                        <label>Email</label>
                        <input type="text" name="email" value="${ user.getEmail() }">
                    </div>

                    <div class="input icon-fingerprint">
                        <label>Password</label>
                        <input type="password" name="password" placeholder="password">
                    </div>

                    <div class="input submit">
                        <input type="submit" value="Save" />
                    </div>
                </form>
            </div>
            <div class="info-block">
                <p><strong>${ user.getName() }</strong> are you a restaurant owner? If so, <a href="/JustDine/restaurant?page=new">click here to add it</a> to our listings.</p>
            </div>
        </div>
    </jsp:attribute>
</justdine:page>
