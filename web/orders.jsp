<%-- 
    Document   : orders
    Created on : Feb 25, 2015, 3:52:02 PM
    Author     : Adrian
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="justdine" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:set var="pageTitle" value="Order"></c:set>

<c:if test="${forUser == null}">
    <c:set var="currentRestaurant" value="${ orders.toArray()[0].getFKRestaurant() }"></c:set>
    <c:set var="pageTitle" value="Orders for ${ currentRestaurant.getName() }"></c:set>
    <c:set var="forUser" value="${false}" />
</c:if>

<justdine:page pageTitle="${ pageTitle }">
    <jsp:attribute name="pageContent">
        <c:if test="${!forUser}">
            <justdine:restaurant restaurant="${ currentRestaurant }"></justdine:restaurant>
        </c:if>
        <c:choose>
            <c:when test="${ orders != null && orders.size() > 0}">
                <c:forEach var="order" items="${orders}" varStatus="loop">
                    <div class="orders">
                        <c:choose>
                            <c:when test="${ forUser }">
                                <h3 class="sub-title"><span>Order #${loop.index + 1} on the ${ order.getDateAddedString() }</span></h3>
                                <justdine:restaurant restaurant="${ order.getFKRestaurant() }"></justdine:restaurant>
                            </c:when>
                            <c:otherwise>
                                <h3 class="sub-title"><span>Order #${loop.index + 1} on the ${ order.getDateAddedString() } by ${ order.getFKUser().getName() }</span></h3>
                            </c:otherwise>
                        </c:choose>
                        <justdine:orderList full="${ true }" order="${ order }"></justdine:orderList>
                    </div>
                </c:forEach>
            </c:when>
            <c:otherwise>
                <div class="empty-list"><p>No currently pending orders.</p></div>
            </c:otherwise>
        </c:choose>
    </jsp:attribute>
</justdine:page>

