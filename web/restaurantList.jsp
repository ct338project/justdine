<%-- 
    Document   : restaurantMenu
    Created on : Jan 29, 2015, 6:13:34 PM
    Author     : Adrian
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="justdine" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 

<justdine:page pageTitle="Restaurants">
    <jsp:attribute name="pageContent">
        <c:forEach var="restaurant" items="${restaurants}">
            <justdine:restaurant restaurant="${restaurant}"></justdine:restaurant>
        </c:forEach>
    </jsp:attribute>
</justdine:page>
