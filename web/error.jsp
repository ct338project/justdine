<%-- 
    Document   : error
    Created on : 27-Jan-2015, 17:57:54
    Author     : Sean
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="justdine" tagdir="/WEB-INF/tags" %>

<justdine:error errorCode="${ errorCode }" errorMessage="${ errorMessage }" exceptionMessage="${exceptionMessage}"></justdine:error>