<%-- 
    Document   : help
    Created on : 25-Feb-2015, 18:24:20
    Author     : Niall Martin
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="justdine" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 

<justdine:page pageTitle="Help">
    <jsp:attribute name="pageContent">
        <div class="static-webpage">
            <p>
                If you need any help, do not hesitate to contact us for support. You can send any questions you have for us using the information on our contact page.
            </p>
        </div>
    </jsp:attribute>
</justdine:page>
