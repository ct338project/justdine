<%-- 
    Document   : menuAddItem
    Created on : Feb 23, 2015, 6:36:45 PM
    Author     : Adrian
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="justdine" tagdir="/WEB-INF/tags" %>

<justdine:restaurantPage restaurant="${restaurant}" pageName="menu" pageTitle="Add Menu Item">
    <jsp:attribute name="pageContent">
        <form action="" method="post">
            <justdine:errorMessage error="${error}"></justdine:errorMessage>

            <div class="input icon-pencil disabled">
                <label>Category</label>
                <input type="text" disabled value="${ category.getTitle() }">
            </div>

            <div class="input icon-pencil">
                <label>Title</label>
                <input type="text" name="title" placeholder="Grilled Seabass">
            </div>

            <div class="input icon-pencil">
                <label>Sub-Title</label>
                <input type="text" name="subtitle" placeholder="With garlic butter and lemon">
            </div>

            <div class="input icon-pencil">
                <label>Price</label>
                <input class="price" type="text" name="price" placeholder="16.00">
            </div>

            <div class="input submit">
                <input type="submit" value="Submit" />
            </div>
        </form>
    </jsp:attribute>
</justdine:restaurantPage>