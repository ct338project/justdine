<%-- 
    Document   : rating
    Created on : Jan 29, 2015, 6:00:23 PM
    Author     : Adrian
--%>

<%@tag description="Rating tag" pageEncoding="UTF-8"%>
<%@attribute name="rating" required="true"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 

<span class="rating"><strong><c:forEach begin="0" end="4" varStatus="loop">&#9733; <c:if test='${loop.current == rating}'></strong></c:if></c:forEach></span>