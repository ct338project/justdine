<%-- 
    Document   : menuOptions
    Created on : Feb 22, 2015, 11:02:21 PM
    Author     : Adrian
--%>

<%@tag description="Menu options" pageEncoding="UTF-8"%>
<%@taglib prefix="justdine" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 

<%@attribute name="menuOptions"%>

 <div class="menu-option">
        <div class="option-title">
                <div class="box grid">
                        <h3 class="cell">Edit your order</h3>
                        <button class="icon-button small less light"></button>
                </div>
        </div>
        <div class="options">
            <c:forEach var="menuOption" items="${ menuOptions }">
                <justdine:menuOption option="${ menuOption }"></justdine:menuOption>
            </c:forEach>    
        </div>
</div>