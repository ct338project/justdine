<%-- 
    Document   : menuCategory
    Created on : Feb 22, 2015, 10:35:30 PM
    Author     : Adrian
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@tag description="Menu Category" pageEncoding="UTF-8"%>
<%@taglib prefix="justdine" tagdir="/WEB-INF/tags" %>

<%@attribute name="category" type="entity.MenuCategory"%>
<%@attribute name="restaurant" type="entity.Restaurant"%>

<c:set var="isOwner" value="${ sessionScope.user != null && sessionScope.user.getPkId() == restaurant.getOwner().getPkId() }"></c:set>
<c:set var="menuItems" value="${ category.getMenuItemCollection() }"></c:set>

<section>
    <c:if test="${isOwner}"><div class="add-item"><a href="/JustDine/menu?page=item&action=create&restaurant=${restaurant.getPkId()}&category=${ category.getPkId() }">Add Item</a></div></c:if>
    <h4 class="sub-title"><span>${ category.getTitle() }</span></h4>
    <c:choose>
        <c:when test="${ menuItems.size() > 0 }">
            <c:forEach var="menuItem" items="${ menuItems }">
                <justdine:menuItem restaurant="${ restaurant }" menuItem="${ menuItem }"></justdine:menuItem>
            </c:forEach>
        </c:when>
        <c:otherwise>
            <div class="empty-list"><p>No menu items.</p></div>
        </c:otherwise>
    </c:choose>
</section>