<%-- 
    Document   : restaurant
    Created on : Jan 29, 2015, 5:50:46 PM
    Author     : Adrian
--%>

<%@tag description="put the tag description here" pageEncoding="UTF-8"%>
<%@taglib prefix="justdine" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 

<%@attribute name="restaurant" required="true" type="entity.Restaurant" %>
<%@attribute name="pageName" %>

<c:choose>
    <c:when test="${(order != null && order.getFKRestaurant().getPkId().equals(restaurant.getPkId()))}">
        <c:set var="currentOrder" value="${ order }"></c:set>
    </c:when>
    <c:when test="${ user.getOrderFromRestaurant(restaurant) != null}">
        <c:set var="currentOrder" value="${ user.getOrderFromRestaurant(restaurant) }"></c:set>
    </c:when>
    <c:otherwise>
        <c:set var="currentOrder" value="${ null }"></c:set>
    </c:otherwise>
</c:choose>
            


<div class="restaurant grid">
    <div class="thumb">
            <img src="${restaurant.photo}">
    </div>
    <div class="description cell">
        <h3><a href="/JustDine/restaurant?id=${restaurant.pkId}">${restaurant.name}</a></h3>
        <h4><justdine:rating rating='${restaurant.rating}'></justdine:rating> ${restaurant.category}</h4>
        <p>${restaurant.description}</p>
    </div>
    <div class="meta">
            <div class="opening-times grid">
                    <div class="icon">
                            <img src="/JustDine/assets/icon/clock-icon.svg">
                    </div>
                    <div class="cell">
                            <h4>Open Now</h4>
                            <h5>9:00 - 22:00</h5>
                    </div>
            </div>
            <div class="availability grid">
                    <div class="icon">
                            <img src="/JustDine/assets/icon/clipboard-icon.svg">
                    </div>
                    <div class="cell">
                            <h4>Available</h4>
                            <h5>Open for reservations</h5>
                    </div>
            </div>
            <div class="buttons grid">
                    <div class="cell"><a href="/JustDine/restaurant?id=${restaurant.pkId}" class='icon-button menu <c:if test="${pageName == 'menu'}">active</c:if>'>Menu</a></div>
                    <div class="cell"><a href="/JustDine/restaurant?id=${restaurant.pkId}&page=order" <c:if test="${ currentOrder != null}">data-notification-count="${ currentOrder.getItemCount() }"</c:if> class='icon-button order <c:if test="${pageName == 'order' || currentOrder != null}">active</c:if>'>Order</a></div>
                    <div class="cell"><a href="/JustDine/restaurant?id=${restaurant.pkId}&page=info" class='icon-button info <c:if test="${pageName == 'info'}">active</c:if>'>Info</a></div>
            </div>
    </div>
</div>