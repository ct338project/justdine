<%-- 
    Document   : base
    Created on : Jan 29, 2015, 5:42:23 PM
    Author     : Adrian
--%>

<%@tag description="Base HTML page with header and footer." pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 

<!DOCTYPE html>
<html>
<head>
	<title>Just Dine</title>
	<link rel="stylesheet" href="/JustDine/assets/style/style.css">
</head>
<body>
<header class="header">
	<div class="container grid">
		<div>
			<h1 class="logo"><img src="/JustDine/assets/svg/Logo.svg" alt="Just Dine"></h1>
		</div>
		<div class="cell">
			<nav>
				<ul>
					<li>
						<h3>View</h3>
						<h2><a href="/JustDine/restaurants">Restaurants</a></h2>
					</li>
					<li>
						<h3>Your</h3>
						<h2><a href="/JustDine/orders">Orders</a></h2>
					</li>
					<li>
						<h3>Write</h3>
						<h2><a href="/JustDine/review">Review</a></h2>
					</li>
					<li>
						<h3>Try</h3>
						<h2><a href="/JustDine/search">Search</a></h2>
					</li>
				</ul>
                            <p><c:choose><c:when test="${sessionScope.user != null}">Hi <a class="profile-link" href="/JustDine/profile">${sessionScope.user.getName()}</a> &mdash; <a href="/JustDine/logout">logout</a></c:when><c:otherwise><a href="/JustDine/login">Login</a> <a href="/JustDine/registration">Sign Up</a></c:otherwise></c:choose> <a href="/JustDine/about">About</a> <a href="/JustDine/contact">Contact</a> <a href="/JustDine/help">Help</a></p>
			</nav>
		</div>
	</div>
</header>
<div class="container">
      <jsp:doBody/>
</div>

<footer>
	<div class="container">
		<p>&copy; JustDine 2014</p>
	</div>
</footer>
</body>
<script src="/JustDine/assets/script/build.js" type="text/javascript"></script>
</html>