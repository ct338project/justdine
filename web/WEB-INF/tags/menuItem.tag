<%-- 
    Document   : menuItem
    Created on : Feb 22, 2015, 10:33:30 PM
    Author     : Adrian
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@tag description="MenuItem" pageEncoding="UTF-8"%>

<%@attribute name="menuItem" type="entity.MenuItem" %>
<%@attribute name="restaurant" type="entity.Restaurant" %>

<div class="menu-item grid">
        <div class="cell">
                <h4 class="price">&euro;${ menuItem.getFixedPrice() }</h4>
                <h3><span>${ menuItem.getTitle() }</span></h3>
                <p>${ menuItem.getDescription() }</p>
        </div>
        <div class="button">
            <c:choose>
                <c:when test="${ order != null && order.hasItem(menuItem) }">
                    <button class="icon-button add disabled">Add</button>
                </c:when>
                <c:otherwise>
                    <a href="/JustDine/restaurant?page=order&action=add&restaurant=${ restaurant.getPkId() }&item=${ menuItem.getPkId() }" class="icon-button add">Add</a>
                </c:otherwise>
            </c:choose>
        </div>
</div>