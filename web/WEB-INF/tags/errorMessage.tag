<%-- 
    Document   : errorMessage
    Created on : Feb 22, 2015, 7:28:24 PM
    Author     : Adrian
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@tag description="Error block" pageEncoding="UTF-8"%>

<%-- The list of normal or fragment attributes can be specified here: --%>
<%@attribute name="error" type="Exception"%>

<c:if test="${ error != null}"><div class="error"><p>${ error.getMessage() }</p></div></c:if>