<%-- 
    Document   : error
    Created on : Feb 4, 2015, 11:38:33 AM
    Author     : Adrian
--%>

<%@tag description="Restaurant page" pageEncoding="UTF-8"%>
<%@taglib prefix="justdine" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 

<%@attribute name="errorMessage" %>
<%@attribute name="errorCode" %>
<%@attribute name="exceptionMessage" %>


<justdine:base>
     <jsp:body>
         <div class="error-page">
             <h2><c:if test="${ errorCode != null }">${ errorCode }</c:if><c:if test="${ errorCode == null}">404</c:if></h2>
             <h3><c:if test="${ errorMessage != null }">${ errorMessage }</c:if><c:if test="${ errorMessage == null}">Not found</c:if></h3>
             <c:if test="${ exceptionMessage != null && exceptionMessage.length() > 0 }"><p>(${exceptionMessage})</p></c:if>
             <p><a href="/JustDine/">Go Home</a></p>
         </div>
    </jsp:body>
</justdine:base>