<%-- 
    Document   : orderList
    Created on : Feb 24, 2015, 8:59:26 PM
    Author     : Adrian
--%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<%@tag description="List an orders contents" pageEncoding="UTF-8"%>

<%-- The list of normal or fragment attributes can be specified here: --%>
<%@attribute name="order" type="entity.CustomerOrder"%>
<%@attribute name="full" type="java.lang.Boolean" %>
<c:if test="${full == null}"><c:set var="full" value="${false}" /></c:if>

<div class="order-content">
    <c:choose>
        <c:when test="${ order != null && order.getMenuItemCollection().size() > 0}">
            <c:forEach var="menuItem" items="${ order.getMenuItemCollection() }">
                <p>${ menuItem.getTitle() } <a href="/JustDine/restaurant?page=order&action=delete&order=${ order.getPkId() }&item=${ menuItem.getPkId() }" class="edit-order">Remove</a></p>
                <c:if test="${ full }"><p class="description">${ menuItem.getDescription() }</p></c:if>
            </c:forEach>
           <div class="order-total"><span>&euro;${ order.getFixedOrderTotal() }</span></div> 
        </c:when>
        <c:otherwise>
            <div class="empty-list inverted"><p>No items.</p></div>
           <div class="order-total"><span>&euro;0.00</span></div> 
        </c:otherwise>
    </c:choose>
</div>