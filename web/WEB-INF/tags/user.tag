<%-- 
    Document   : user
    Created on : Feb 25, 2015, 4:32:13 PM
    Author     : Adrian
--%>

<%@tag description="User view" pageEncoding="UTF-8"%>

<%@attribute name="user" type="entity.User"%>

<div class="user">
    <h3>${ user.getName() }</h3>
    <h4>${ user.getEmail() }</h4>
</div>