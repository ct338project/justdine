<%-- 
    Document   : menuOption
    Created on : Feb 22, 2015, 11:03:10 PM
    Author     : Adrian
--%>

<%@tag description="put the tag description here" pageEncoding="UTF-8"%>

<%@attribute name="option" required="true"%>

<div class="option grid">
        <p class="cell">Which side would you like with your meal?</p>
        <div class="option-value">
                <select name="steak-cooked">
                        <option value="salad">Salad</option>
                        <option value="chips">Chips</option>
                        <option value="bap">Bap</option>
                </select>
        </div>
</div>