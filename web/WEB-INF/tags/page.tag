<%-- 
    Document   : page
    Created on : Feb 20, 2015, 11:38:33 AM
    Author     : Adrian
--%>

<%@tag description="Default page" pageEncoding="UTF-8"%>
<%@taglib prefix="justdine" tagdir="/WEB-INF/tags" %>

<%@attribute name="pageTitle" required="true" %>
<%@attribute name="pageContent" fragment="true" %>


<justdine:base>
     <jsp:body>
        <div class="page">
            <header class="title">
                <h3>${pageTitle}</h3>
            </header>
        
            <jsp:invoke fragment="pageContent"/>
        </div>
    </jsp:body>
</justdine:base>