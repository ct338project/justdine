<%-- 
    Document   : restaurantPage
    Created on : Jan 29, 2015, 5:46:42 PM
    Author     : Adrian
--%>

<%@tag description="Restaurant page" pageEncoding="UTF-8"%>
<%@taglib prefix="justdine" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 

<%@attribute name="pageName" required="true" %>
<%@attribute name="pageTitle" required="true" %>
<%@attribute name="restaurant" required="true" type="entity.Restaurant" %>
<%@attribute name="pageContent" fragment="true" %>



<justdine:base>
     <jsp:body>
        <c:if test="${ sessionScope.user != null && sessionScope.user.getPkId() == restaurant.getOwner().getPkId() }">
        <div class="owner">
            <h3>Hi there, ${ sessionScope.user.getName() }. You own this restaurant.</h3>
            <ul>
                <li><a href="/JustDine/orders?page=restaurant&restaurant=${ restaurant.getPkId() }">View your restaurants orders</a></li>
                <li><a href="#">Edit your restaurants details</a></li>
                <li><a href="#">Delete your restaurant</a></li>
            </ul>
        </div>
        </c:if>
        <justdine:restaurant pageName="${pageName}" restaurant="${restaurant}"></justdine:restaurant>
	<div class="restaurant-page ${pageName}">
            <header class="title">
                <h3>${pageTitle}</h3>
            </header>
            <div class="content">
                <jsp:invoke fragment="pageContent"/>
            </div>
        </div>
    </jsp:body>
</justdine:base>