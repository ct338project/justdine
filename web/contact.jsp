<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="justdine" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 

<justdine:page pageTitle="Contact Us">
    <jsp:attribute name="pageContent">
        <div class="static-webpage">
            <p>
                If you have any queries, you can contact us using the details below:
            </p>
            <p>
                Phone: 087 1234567        
            </p>
            <p>
                Email: contact@justdine.com
            </p>
        </div>
    </jsp:attribute>
</justdine:page>

